/**
 * Created by asci on 1/17/16.
 */
var express=require('express');
var bodyParser=require('body-parser');
var morgan=require('morgan');
var server_config=require('./server_config');
var mongoose=require('mongoose');
var fs=require('fs');
var port=server_config.port;
var cron=require('cron');
//connecting the database
if(server_config.dev){
    var _db=server_config.database.dev;
}else{
    var _db=server_config.database.production;
}
mongoose.connect(_db,function(err){
   if(err){
       console.log(err);
   }else{
       if(server_config.dev){
           console.log("Successfully connected to the database at "+_db );
       }else{
           console.log("Successfully connected to the database");
       }

   }
});

//starting the cron job
var cronJobList=require(server_config.backend+'cronJobList');

var job=new cron.CronJob({
    cronTime:'00 00 9 * * *',
    onTick:cronJobList.pendingNotification,
    start:true
});

//declaring the server
var app=express();
var http=require('http').Server(app);
var io=require('socket.io')(http);

//using middleware
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));

//creating morgan date_type token
morgan.token('date_type',function(){
    return Date().toLocaleString();
});

//custom logger format
morgan.format('custom', '[:date_type] :method :url :status :res[content-length] - :response-time ms')

/***** if you want to log it *****/
var LogStream = fs.createWriteStream(server_config.baseFolder + '/server_log.log', {flags: 'a'})
if(server_config.dev){
    app.use(morgan('custom'));
}else{
    app.use(morgan('custom'));//,{stream:LogStream}));
}

//attaching the staticFolder
app.use(express.static(server_config.staticFolder));

//adding the api files
var api=require(server_config.backend+'api')(app,express,io);
app.use('/api',api);

// * catches all routes
app.get('*',function(req,res){
   res.sendFile(server_config.staticFolder+'views/index.html');
});

http.listen(port,function(err){
    if(err){
        console.log(err);
    }else{
        console.log("Server Started at port:"+port);
    }
});
