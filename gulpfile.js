/**
 * Created by asci on 1/18/16.
 */
var gulp=require('gulp');
var gulp_config=require('./gulp_config.json');

gulp.task('transfer-html',function(){
   return gulp.src(gulp_config.source.templates,{base:gulp_config.base.source})
       .pipe(gulp.dest(gulp_config.dest.templates));
});

gulp.task('transfer-js',function(){
    return gulp.src(gulp_config.source.scripts,{base:gulp_config.base.source})
        .pipe(gulp.dest(gulp_config.dest.scripts));
});

gulp.task('transfer-styles',function(){
    return gulp.src(gulp_config.source.styles,{base:gulp_config.base.source})
        .pipe(gulp.dest(gulp_config.dest.styles));
});
gulp.task('transfer-images',function(){
    return gulp.src(gulp_config.source.images,{base:gulp_config.base.source})
        .pipe(gulp.dest(gulp_config.dest.images));
});

gulp.task('transfer-font',function(){
    return gulp.src(gulp_config.source.fonts,{base:gulp_config.base.source})
        .pipe(gulp.dest(gulp_config.dest.fonts));
})

gulp.task('watch',function(){
   gulp.watch(gulp_config.source.templates,['transfer-html']);
    gulp.watch(gulp_config.source.scripts,['transfer-js']);
    gulp.watch(gulp_config.source.styles,['transfer-styles']);
    gulp.watch(gulp_config.source.images,['transfer-images']);
    gulp.watch(gulp_config.source.fonts,['transfer-font']);
});

gulp.task('default',['transfer-html','transfer-js','transfer-styles','transfer-images','transfer-font','watch']);