/**
 * Created by asci on 1/19/16.
 */
(function(){
    var userService=angular.module('userService',[]);
    userService.factory('userServiceFactory',function($http){
        var vm={};
        vm.getAllUser=function(){
            return $http.get('/api/all_users');
        };

        vm.addLocation=function(locationData){
            return $http.post('/api/addLocation',locationData);
        };
        vm.getLocation=function(){
            return $http.get('/api/getLocation');
        };
        vm.PasswordChange=function(changeData){
            return $http.post('/api/changePassword',changeData);
        };
        vm.getUserDetail=function(user_id){
          return $http({
            method:'GET',
            url:'/api/getUserDetail',
            params:{user_id:user_id}
          });
        };
        return vm;
    });

})();
