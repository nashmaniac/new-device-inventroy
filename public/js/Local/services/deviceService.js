/**
 * Created by asci on 1/19/16.
 */
(function(){
    var deviceService=angular.module('deviceService',[]);
    deviceService.factory('deviceServiceFactory',function($http){
        var vm={};
        vm.exportSpecificDataTypes=function(deviceDataTypeID){
          console.log(deviceDataTypeID);
          return $http({
            url:'/api/export',
            method:'GET',
            params:{data_id:deviceDataTypeID}
          });
        };
        vm.addNewDeviceType=function(deviceTypeData){
            return $http.post('/api/addDeviceType',deviceTypeData);
        };
        vm.getAllDeviceType=function(){
            return $http.get('/api/getAllDeviceType');
        };
        vm.addDevice=function(deviceData){
            return $http.post('/api/addDevice',deviceData);
        };
        vm.editDevice=function(deviceData){
            return $http.post('/api/editDevice',deviceData);
        };
        vm.getAllDevice=function(data){
            if(data){
                return $http({
                    url:'/api/getDevice',
                    method:'GET',
                    params:{filter:data}
                });
            }else{
                return $http({
                    url:'/api/getDevice',
                    method:'GET',
                    params:{filter:[]}
                });
            }
        };
        vm.addData=function(dataData){
            return $http.post('/api/addData',dataData);
        };
        vm.getAllData=function(){
            return $http.get('/api/allDataType');
        };
        vm.getDeviceDetail=function(device_id){
            return $http.get('/api/deviceDetail/'+device_id)
        };
        vm.getDeviceForCheckoutAutoSuggestion=function(filter_tag){
            return $http.get('/api/getDeviceInfoForCheckoutFilter/'+filter_tag);
        };

        vm.submitBorrowEntry=function(borrowData){
            return $http.post('/api/submitBorrowEntry',borrowData);
        };
        vm.getBorrowInfoForDevice=function(device_id){
            return $http.get('/api/getBorrowInfoForDevice/'+device_id);
        };

        vm.deviceCheckIn=function(checkinInfo){
            return $http.post('/api/deviceCheckIn',checkinInfo);
        };
        vm.getBorrowHistory=function(device_id){
            return $http.get('/api/getBorrowHistory/'+device_id);
        };
        vm.getBorrowDetail=function(borrow_id){
            return $http.get('/api/getBorrowDetail/'+borrow_id);
        };
        vm.autoSuggestionsDevice=function(){
            return $http.get('/api/autoSuggestions');
        };
        vm.getBorrowedDeviceList=function(user_id){
            return $http({
                url:'/api/getBorrowedDevice',
                method:"GET",
                params:{
                    user_id:user_id
                }
            });
        };
        vm.getDeviceByLocation=function(location_id){
            return $http({
                url:'/api/getDeviceByLocation',
                method:"GET",
                params:{
                    location_id:location_id
                }
            });
        };
        vm.getSpecificDeviceType=function(deviceTypeId){
            return $http({
                url:'/api/getSpecificDeviceType',
                method:'GET',
                params:{
                    deviceTypeId:deviceTypeId
                }
            });
        };
        vm.editNewDeviceType=function(deviceData){
            return $http.post('/api/editNewDeviceType',deviceData);
        };
        return vm;

    });
})();
