/**
 * Created by asci on 1/19/16.
 */
(function(){
    var loginService=angular.module('loginService',[]);

    loginService.factory('Auth',function($http,AuthToken,$q){
        var vm={};
        vm.login=function(loginData){
            return $http.post('/api/login',loginData)
                .success(function(data){
                    AuthToken.setToken(data.token);
                    return data;
                });
        };

        vm.logout=function(){
            AuthToken.setToken();
        };

        vm.isLoggedIn=function(){
            if(AuthToken.getToken()){
                return true;
            }else{
                return false;
            }
        };

        vm.getUser=function(){
            if(AuthToken.getToken()){
                return $http.get('/api/me');
            }else{
                return $q.reject({
                   message:"User has no authentication"
                });
            }
        };

        vm.resetPasswordToEmail=function(username){
          return $http.post('/api/sendPasswordToMail',{username:username});
        };
        return vm;
    });

    loginService.factory('AuthToken',function($window){
        var vm={};
        vm.setToken=function(token){
            if(token){
                $window.localStorage.setItem('token',token);
            }else{
                $window.localStorage.removeItem('token');
            }
        };
        vm.getToken=function(){
            return $window.localStorage.getItem('token');
        };
        return vm;
    });

    loginService.factory('AuthIntercept',function(AuthToken,$location,$q){
        var interceptFactory={};

        interceptFactory.request=function(config){
            var token=AuthToken.getToken();
            if(token){
                config.headers['x-access-token']=token;
            }
            return config;
        };

        interceptFactory.responseError=function(response){
            if(response.status==403){
                $location.path('/')
            }
            return $q.reject(response);
        };
        return interceptFactory;
    });

})();
