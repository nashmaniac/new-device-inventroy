(function(){
  var globalUsageController=angular.module('GlobalUsage',[]);
  globalUsageController.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
  }]);
  globalUsageController.service('fileUpload', ['$http','ngNotify',function ($http,ngNotify) {
    this.uploadFileToUrl = function(file, uploadUrl,data_id){
        var fd = new FormData();
        fd.append('file', file);
        return $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
    }
  }]);

})();
