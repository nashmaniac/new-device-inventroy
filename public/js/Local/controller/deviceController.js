/**
 * Created by asci on 1/19/16.
 */
(function(){
    var deviceController=angular.module('deviceController',['bw.paging','deviceService','userService','GlobalUsage']);
    deviceController.filter('propsFilter', function() {
        return function(items, props) {
            var out = [];

            if (angular.isArray(items)) {
                items.forEach(function(item) {
                    var itemMatches = false;

                    var keys = Object.keys(props);
                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        }
    });

    deviceController.controller('deviceCtrl',function(userServiceFactory,deviceServiceFactory,$location,socketio,ngNotify,fileUpload,$scope){
        var vm=this;
        vm.currentPage=1;
        vm.pageLimit=1;
        vm.searchMode=true;
        vm.dynamicValues={};
        userServiceFactory.getLocation().success(function(data){
            vm.locations=data;
        });
        vm.allDevice=[];
        vm.filteredDevice=[];
        vm.totalFilter=[];
        vm.range=10;
        vm.goBack=function(){
          vm.searchMode=true;
          vm.deviceDetail={};
          vm.bringDevice(vm.totalFilter);
        };
        vm.changeMode=function(device_id){
          vm.searchMode=false;
          deviceServiceFactory.getDeviceDetail(device_id)
              .success(function(data){
                  vm.deviceDetail=data;
                  vm.deviceProperties=[];
                  var log = [];
                  angular.forEach(vm.deviceDetail.properties, function(value, key) {
                      this.push(key + ': ' + value);
                  }, vm.deviceProperties);
              });
        };
        vm.onUiSelectValue=function(data){
            vm.currentPage=1;
            vm.totalFilter.push({key:data.key,value:data.value,filter:'AND'});
            vm.filterValues=undefined;
            vm.range=10;
            vm.bringDevice(vm.totalFilter);
        };
        vm.setRange=function(){
          vm.range=10;
        };
        vm.changeRange=function(){
          console.log('will be changed');
        }
        vm.bringDevice=function(filter){
            vm.allDevice=[];
            vm.borrow_list=[];
            vm.searching=true;
            deviceServiceFactory.getAllDevice(filter)
                .success(function(data){
                    vm.searching=false;
                    vm.allDevice=data['devices'];
                    vm.borrow_list=data['borrow'];
                    vm.doPaging();
                });
        };
        vm.removeFilter=function(event){
          var tag=angular.element(event.target).closest('div').text().trim().split(" - ");
          angular.element(event.target).closest('td').remove();
          //remove the element in the totalFilter
          for(var i=0;i<vm.totalFilter.length;i++){
            if(vm.totalFilter[i].key==tag[0] && vm.totalFilter[i].value==tag[1]){
              vm.totalFilter.splice(i,1);
              break
            }
          }
          vm.bringDevice(vm.totalFilter);

        }
        vm.ToggleValue=function(event){
          var tag=angular.element(event.target).closest('div').find('div').text().trim().split(" - ");
          //remove the element in the totalFilter

          for(var i=0;i<vm.totalFilter.length;i++){
            if(vm.totalFilter[i].key==tag[0] && vm.totalFilter[i].value==tag[1]){
              if(angular.element(event.target).text()=='AND'){
                vm.totalFilter[i].filter='OR';
              }else{
                vm.totalFilter[i].filter='AND';
              }
              break
            }
          }
          vm.bringDevice(vm.totalFilter);
        };
        vm.ClearFilter=function(){
            vm.filterValues=undefined;
            vm.totalFilter=[];
            vm.bringDevice();
        };
        vm.bringDevice();
        deviceServiceFactory.autoSuggestionsDevice()
            .success(function(data){
                vm.autoSuggestions=data;
            });
        deviceServiceFactory.getAllDeviceType()
            .success(function(data){
                vm.deviceTypeColumn=['Device Type','Fields','Data Type'];
                vm.deviceTypes=data;
            });
        vm.doPaging=function(){
            var s=(vm.currentPage-1)*parseInt(vm.pageLimit,10);
            var e=(vm.currentPage-1)*parseInt(vm.pageLimit,10)+parseInt(vm.pageLimit,10);
            var t=vm.allDevice.slice(s,e);
            // for(var i=s;i<e;i++){
            //     t.push(vm.allDevice[i]);
            // }
            vm.filteredDevice= t.filter(function(e){
                return e!=undefined;
            });
        };
        vm.pageItem=function(page){
            vm.currentPage=page;
            vm.doPaging();

        };
        vm.onlimitchange=function(){
            if(vm.pageLimit!=''){
                vm.currentPage=1;
                vm.doPaging();
            }
        }
        vm.onValueChange=function(){
            vm.dynamicValues={};
            if(vm.type==undefined){
                vm.custom_fields=[];
            }else{
                var v=vm.deviceTypes.filter(function(el){
                    return el._id==vm.type._id
                });
                if(v.length>0){
                    vm.custom_fields= v[0].custom_fields;
                }
            }

        };
        vm.onHighlight=function(key,value,id){
          var f=vm.autoSuggestions.filter(function(el){
            return el.key==key && el.value==value && el._id==id;
          });
          if (vm.autoSuggestions.indexOf(f[0])%10==9){
            var t=parseInt(vm.autoSuggestions.indexOf(f[0])/10,10)+1;
            vm.range=t*10+10;
          };
        };
        vm.addNewDevice=function(){
            deviceServiceFactory.addDevice({
                type:vm.type,
                properties:vm.dynamicValues,
                location:vm.selectedLocation._id
                })
                .success(function(data){
                    if(data.success){
                        ngNotify.set(data.message,{
                            type:'success'
                        });
                    }else{
                        ngNotify.set(data.message,{
                            type:'error'
                        })
                    }
                    $location.path('/device');
                });

        };
        vm.uploadDataFile=function(){
          var file = $scope.myFile;
          var uploadUrl = '/api/importData';
          fileUpload.uploadFileToUrl(file, uploadUrl).success(function(data){
            if(data.success){
              ngNotify.set(data.message,{
                type:'success'
              });
            }else{
              ngNotify.set(data.message,{
                type:'error'
              })
            }
            $location.path('/device');
          });
        };
        /*socketio.on('device',function(data){
            vm.allDevice.push(data);
        });
        */
    });
    deviceController.controller('deviceTypeCtrl',function(deviceServiceFactory,$location,ngNotify,$window){
        var vm=this;
        vm.choice=[{'id':"custom1"}];

        deviceServiceFactory.getAllDeviceType()
            .success(function(data){
                vm.deviceTypeColumn=['Device Type','Fields','Data Type'];
                vm.deviceTypes=data;
                vm.deviceExportColumn=['Device Type','Action'];
            });

        deviceServiceFactory.getAllData()
            .success(function(data){
                vm.DataType=data;
            });

        vm.addNew=function(){
            var new_id=vm.choice.length+1;
            console.log(new_id);
            vm.choice.push({id:"custom"+new_id});
        };
        vm.removeLast=function(){
            var lastItem=vm.choice.length-1;
            vm.choice.splice(lastItem);
        };

        vm.submitForm=function(){
            var deviceData={
                deviceType:vm.deviceType,
                customField:vm.choice
            };
            console.log(deviceData);
            deviceServiceFactory.addNewDeviceType(deviceData)
                .success(function(data){
                    if(data.success){
                        ngNotify.set(data.message,{
                            type:'success'
                        });
                    }else{
                        ngNotify.set(data.message,{
                            type:'error'
                        })
                    }
                    $location.path('/deviceType')
                });
        };
        vm.exportData=function(data_id){
          deviceServiceFactory.exportSpecificDataTypes(data_id).success(function(data){
              if(data.success){
                console.log(data.filename);
                ngNotify.set('File name: '+data.filename,{
                  type:'success'
                });
                $window.open('/'+data.location,'_blank');
              }
          });
        };
    });
    deviceController.controller('dataCtrl',function(deviceServiceFactory,$location,socketio,ngNotify){
        var vm=this;
        deviceServiceFactory.getAllData()
            .success(function(data){
                vm.dataColumn=['Data Type Name','Text Box Type','Values'];
                vm.allData=data;
            });
        vm.choice=[];
        vm.selectValues={}
        vm.dataTypeToTrack=['Text','Number','Select','Boolean'];
        vm.onChangeValue=function(){
            if(vm.metaType=='select'){
                vm.choice=[];
                vm.choice.push({id:'custom1'});
            }else{
                vm.choice=[];
                vm.selectValues={};
            }
        };
        vm.addOption=function(){
            var d=vm.choice.length+1;
            vm.choice.push({id:'custom'+d});
        };
        vm.removeOption=function(){
            var lastItem=vm.choice.length-1;
            vm.choice.splice(lastItem);
        };
        vm.submitData=function(){
            var t=[];
            if(Object.keys(vm.selectValues).length==0 && vm.metaType=='select'){
                ngNotify.set('You are supposed to supply some options',{
                    type:'info'
                });
                return false;
            }else{
                for(var e in vm.selectValues){
                    t.push(vm.selectValues[e]);
                }
            }
            deviceServiceFactory.addData({
                dataTypeName:vm.dataName,
                textBoxType:vm.metaType,
                selectValue:t
            }).success(function(data){
                if(data.success){
                    ngNotify.set(data.message,{
                        type:'success'
                    });
                    $location.path('/dataType');
                }else{
                    ngNotify.set(data.message,{
                        type:'error'
                    });
                }
            });

        };
    });
    deviceController.controller('deviceDetailCtrl',function(deviceServiceFactory,$location,$stateParams){
        var vm=this;
        var device_id=$stateParams['id'];
        deviceServiceFactory.getDeviceDetail(device_id)
            .success(function(data){
                vm.deviceDetail=data;
                vm.deviceProperties=[];
                var log = [];
                angular.forEach(vm.deviceDetail.properties, function(value, key) {
                    this.push(key + ': ' + value);
                }, vm.deviceProperties);
            });
    });
    deviceController.controller('checkoutController',function(userServiceFactory,deviceServiceFactory,$location,ngNotify){
        var vm=this;

        userServiceFactory.getAllUser()
            .success(function(data){
                vm.users=data;
            });
        deviceServiceFactory.getDeviceForCheckoutAutoSuggestion(false).success(function(data){
            vm.devices=data;
        });
        vm.device_id_change=function(){
            deviceServiceFactory.getDeviceDetail(vm.selectedDevice.as_id)
                .success(function(data){
                    vm.deviceDetail=data;
                });
        };
        vm.dateChange=function(){
            console.log(vm.duration);
            console.log(Date.now());
            var date=new Date();
            vm.currentDate= new Date(date.getFullYear(),date.getMonth() , date.getDate());
            vm.finalDate= new Date(date.getFullYear(),date.getMonth() , date.getDate()+vm.duration);
        };
        vm.checkoutDevice=function(){
            deviceServiceFactory.submitBorrowEntry({
                device_id:vm.selectedDevice._id,
                user_id:vm.selectedItem._id,
                duration:vm.duration,
                currentDate:vm.currentDate,
                finalDate:vm.finalDate,
                borrow_comment:vm.remarks
            }).success(function(data){
                if(data.success){
                    ngNotify.set(data.message,{
                        type:'success'
                    });
                }else{
                    ngNotify.set(data.message,{
                        type:'error'
                    });
                }
                $location.path('/device');
            });
        };
    });
    deviceController.controller('checkinController',function(deviceServiceFactory,$location,ngNotify){
        var vm=this;
        deviceServiceFactory.getDeviceForCheckoutAutoSuggestion(true).success(function(data){
            vm.devices=data;
        });
        vm.first_time=false;
        vm.device_id_change=function(){
            deviceServiceFactory.getBorrowInfoForDevice(vm.selectedDevice._id).success(function(data){
                vm.borrowData=data;
                deviceServiceFactory.getDeviceDetail(vm.borrowData.device.as_id).success(function(d){
                    vm.deviceSpecificData=d;
                    vm.first_time=true;
                });

            });
        };
        vm.checkinDevice=function(){
            if(vm.lost==undefined){
                vm.lost=false;
            }
            if(vm.return_comment==undefined){
                vm.return_comment='';
            }
            var date=new Date();
            console.log(vm.return_comment);
            deviceServiceFactory.deviceCheckIn({
                borrow_id:vm.borrowData._id,
                device_id:vm.borrowData.device._id,
                lost:vm.lost,
                return_comment: vm.return_comment,
                return_date:new Date(date.getFullYear(),date.getMonth() , date.getDate())
            }).success(function(data){
                if(data.success){
                    ngNotify.set(data.message,{
                        type:'success'
                    });
                }else{
                    ngNotify.set(data.message,{
                        type:'error'
                    });
                }
                $location.path('/device');
            });
        };
    });
    deviceController.controller('borrowHistoryController',function(deviceServiceFactory,$location,ngNotify,$stateParams){
        var vm=this;
        var device_id=$stateParams['dev_id'];
        deviceServiceFactory.getBorrowHistory(device_id).success(function(data){
            vm.history=data;
        });
    });
    deviceController.controller('borrowDetailController',function(deviceServiceFactory,$location,$stateParams){
        var vm=this;
        var borrow_id=$stateParams['b_id'];
        deviceServiceFactory.getBorrowDetail(borrow_id).success(function(data){
            if(data){
                vm.detail=data[0];
                deviceServiceFactory.getDeviceDetail(vm.detail.device.as_id).success(function(data){
                    vm.deviceDetail=data;
                });
            }
        });
    });

    deviceController.controller('locationController',function(userServiceFactory,ngNotify,$location){
        var vm=this;
        vm.currentPage=1;
        vm.pageLimit='10';
        vm.loading=true;
        vm.filteredLocation=[];
        userServiceFactory.getAllUser().success(function(data){
            vm.users=data;
        });
        userServiceFactory.getLocation().success(function(data){
            vm.allLocation=data;
            vm.doPaging();
        });
        vm.doPaging=function(){
          var s=(parseInt(vm.currentPage)-1)*parseInt(vm.pageLimit);
          var e=s+parseInt(vm.pageLimit);
          vm.loading=false;
          vm.filteredLocation=vm.allLocation.slice(s,e);
        }
        vm.pageItem=function(page){
          vm.currentPage=page;
          vm.filteredLocation=[];
          vm.loading=true;
          vm.doPaging();
        };
        vm.changePageLimit=function(){
          vm.currentPage=1;
          vm.filteredLocation=[];
          vm.loading=true;
          vm.doPaging();
        }
        vm.country=['Canada','Bangladesh','India','USA'];
        vm.submitLocation=function(){
            console.log(vm.place);
            console.log(vm.selectedCountry);
            console.log(vm.in_charge.username+vm.in_charge._id);

            if(vm.selectedCountry==undefined){
                ngNotify.set('Country is to be selected',{
                    type:'error'
                });
                return false;
            }
            if(vm.in_charge._id==undefined){
                ngNotify.set('User is to be selected',{
                    type:'error'
                });
                return false;
            }

            userServiceFactory.addLocation({
                place_name:vm.place,
                country:vm.selectedCountry,
                in_charge:vm.in_charge._id
            }).success(function(data){
                if(data.success){
                    ngNotify.set(data.message,{
                        type:'success'
                    });
                }else{
                    ngNotify.set(data.message,{
                        type:'error'
                    });
                }
                $location.path('/location');
            });
        };
    });

    deviceController.controller('personSearchController',function(deviceServiceFactory,userServiceFactory,ngNotify,$location){
        var vm=this;
        vm.currentPage=1;
        vm.pageLimit=5;
        userServiceFactory.getAllUser()
            .success(function(data){
                vm.users=data;
            });
        vm.getBorrowedDevice=function(user_id){
            deviceServiceFactory.getBorrowedDeviceList(user_id).success(function(data){
                vm.allborrowDevice=data;
                vm.doPaging();
            });
        };
        vm.toggle=true;
        vm.toggleValue=function(){
            vm.allborrowDevice=[];
            vm.selectedLocation=undefined;
            vm.selectedPerson=undefined;
            vm.toggle=!vm.toggle;
        };
        userServiceFactory.getLocation().success(function(data){
            vm.locations=data;
        });
        vm.SearchByLocation=function(location_id){
            deviceServiceFactory.getDeviceByLocation(location_id).success(function(data){
                vm.allborrowDevice=data;
                vm.doPaging();
            });
        };
        vm.doPaging=function(){
            var s=(vm.currentPage-1)*parseInt(vm.pageLimit,10);
            var e=(vm.currentPage-1)*parseInt(vm.pageLimit,10)+parseInt(vm.pageLimit,10);
            var t=[]
            for(var i=s;i<e;i++){
                t.push(vm.allborrowDevice[i]);
            }
            vm.filteredDevice= t.filter(function(e){
                return e!=undefined;
            });
        };
        vm.pageItem=function(page){
            vm.currentPage=page;
            vm.doPaging();
        };
        vm.onlimitchange=function(){
            vm.currentPage=1;
            vm.doPaging();
        };
        return vm;
    });
    deviceController.controller('editController',function(userServiceFactory,deviceServiceFactory,$location,ngNotify,$stateParams){
        var vm=this;
        userServiceFactory.getLocation().success(function(data){
                    vm.locations=data;
        });
        vm.device_id=$stateParams['device_id'];
        deviceServiceFactory.getAllDeviceType()
            .success(function(data){
                vm.deviceTypeColumn=['Device Type','Fields','Data Type'];
                vm.deviceTypes=data;
            });

        deviceServiceFactory.getDeviceDetail(vm.device_id)
            .success(function(data){
                vm.device_detail=data;
                vm.type=data.type;
                vm.selectedLocation=data.location;
                vm.dynamicValues=data.properties;
                vm.custom_fields=vm.deviceTypes.filter(function(er){
                    return er.typeName==data.type.typeName;
                })[0].custom_fields;

            });

        vm.onValueChange=function(){
            vm.dynamicValues={};
            if(vm.type==undefined){
                vm.custom_fields=[];
            }else{
                var v=vm.deviceTypes.filter(function(el){
                    return el._id==vm.type._id
                });
                if(v.length>0){
                    vm.custom_fields= v[0].custom_fields;
                }
            }

        };

        vm.editDevice=function(){
            console.log(vm.dynamicValues);
            var t={};
            for(var i=0;i<vm.custom_fields.length;i++){
                if(vm.dynamicValues[vm.custom_fields[i].name]!=undefined && vm.dynamicValues[vm.custom_fields[i].name]!=''){
                    t[vm.custom_fields[i].name]=vm.dynamicValues[vm.custom_fields[i].name];
                }
            }
            deviceServiceFactory.editDevice({
                type:vm.type._id,
                properties:t,
                location:vm.selectedLocation._id,
                device_id:vm.device_id
            }).success(function(data){
                if(data.success){
                    ngNotify.set(data.message,{
                        type:'success'
                    });
                }else{
                    ngNotify.set(data.message,{
                        type:'error'
                    });
                }
            });
        };

    });
    deviceController.controller('editDeviceTypeCtrl',function(deviceServiceFactory,$location,ngNotify,$stateParams){
        var vm=this;
        vm.choice=[{id:"custom1"}];
        vm.deviceTypeId=$stateParams['id'];
        deviceServiceFactory.getAllData()
            .success(function(data){
                vm.DataType=data;
            });
        deviceServiceFactory.getSpecificDeviceType(vm.deviceTypeId)
            .success(function(data){
                vm.deviceTypeDetail=data;
                vm.deviceType=data.typeName;
                vm.choice=[];
                for(var i=0;i<data.custom_fields.length;i++){
                    vm.choice.push({
                        id:"custom"+(i+1),
                        fieldType:data.custom_fields[i].fieldType,
                        name:data.custom_fields[i].name
                    });
                }
            });
        vm.addNew=function(){
            var new_id=vm.choice.length+1;
            console.log(new_id);
            vm.choice.push({id:"custom"+new_id});
        };
        vm.removeLast=function(){
            var lastItem=vm.choice.length-1;
            vm.choice.splice(lastItem);
        };
        vm.submitForm=function(){
            var deviceData={
                deviceType:vm.deviceType,
                customField:vm.choice,
                _id:vm.deviceTypeId
            };
            console.log(deviceData);
            deviceServiceFactory.editNewDeviceType(deviceData)
                .success(function(data){
                    if(data.success){
                        ngNotify.set(data.message,{
                            type:'success'
                        });
                    }else{
                        ngNotify.set(data.message,{
                            type:'error'
                        })
                    }
                });
        };
    });
})();
