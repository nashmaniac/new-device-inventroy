/**
 * Created by asci on 1/27/16.
 */
var emailjs=require('emailjs');
var server_config=require('../server_config');
module.exports={
    getMailObject:function(){
        var e=emailjs.server.connect({
            user:    server_config.adminAccount,
            password:server_config.adminPass,
            host: server_config.mailServer,
            ssl:     true
        });
        return e;
    },
    sendMail:function(from,to,cc,subject,text){
        console.log('Sending Mail');
        var e=this.getMailObject();
        e.send({
            text:   text,
            from:    from,
            to:      to,
            cc:      cc,
            subject: subject,
            attachment:
                [
                    {data:text, alternative:true},
                ]
        }, function(err, message) {
            if(err) {
                console.log(err);
            }else{
                console.log("Successfully send the mail");
            }
        });
        console.log("Sending Mail Completed");
    }
}