/**
 * Created by asci on 1/27/16.
 */
var borrowEntry=require('./models/borrowEntry');
var moment=require('moment');
var emailModule=require('./emailModule');
var server_config=require('../server_config');
module.exports={
    pendingNotification:function(){
        borrowEntry.find({actual_return:{$exists:false}})
            .populate('device')
            .populate('borrower')
            .populate('in_charge')
            .exec(function(err,d) {
                for(var i=0;i< d.length;i++){
                    var e=d[i].expected_return;
                    var t=moment([e.getFullYear(),e.getMonth(),e.getDate()])
                    var n=moment();
                    var m=t.diff(n,'days');
                    if(m<3){
                        if(m<0){
                            if(-m==1){
                                var tg= -m+" day Late";
                            }else{
                                var tg= -m+" days Late";
                            }
                            var subject="Device Deposit Notification: "+tg;
                        }else{
                            if(m==1){
                                var tg= m+" day Remaining";
                            }else{
                                var tg= m+" days Remaining";
                            }
                            var subject="Device Deposit Notification: "+tg;
                        }
                        console.log(d[i].borrower.email);
                        var message="<table><caption><b>Borrow Details</b></caption>" +
                            "<tr><td>Device ID:</td><td>"+d[i].device.as_id+"</td></tr>"+
                            "<tr><td>Expected Return:</td><td>"+d[i].expected_return+"</td></tr></table>";
                        console.log(message);
                        emailModule.sendMail(server_config.adminAccount,d[i].borrower.email,'',subject,message);
                    }
                }
            });
    }
};

