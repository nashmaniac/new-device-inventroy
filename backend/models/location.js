/**
 * Created by asci on 1/27/16.
 */
var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var locationSchema=new Schema({
    place_name:{type:String,required:true,index:{unique:true}},
    country:{type:String,required:true},
    in_charge:{type:Schema.Types.ObjectId,ref:'User',required:true}
});

module.exports=mongoose.model('location',locationSchema);