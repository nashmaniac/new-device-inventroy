/**
 * Created by asci on 1/21/16.
 */
var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var borrowEntrySchema=new Schema({
    borrower:{type:Schema.Types.ObjectId,ref:'User',required:true},
    device:{type:Schema.Types.ObjectId,ref:'Device',required:true},
    borrow_date:{type:Date,default:Date.now},
    expected_return:{type:Date},
    actual_return:{type:Date},
    borrow_comment:{type:String,default:''},
    return_comment:{type:String,default:''},
    lost: {type:Boolean, default:false},
    in_charge:{type:Schema.Types.ObjectId,ref:'User',required:true},
    received_by:{type:Schema.Types.ObjectId,ref:'User'}
});
module.exports= mongoose.model('borrowEntry',borrowEntrySchema);