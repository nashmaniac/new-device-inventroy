/**
 * Created by asci on 1/17/16.
 */
var mongoose=require('mongoose');
var bcrypt=require('bcrypt-nodejs');
var Schema=mongoose.Schema;

var userSchema=new Schema({
    name: {type:String},
    username:{type:String,required:true,index:{unique:true}},
    password: {type: String, required: true, select: false},
    adminLevel:{type:Boolean,default:false},
    designation:{type:Schema.Types.ObjectId,ref:'Designation'},
    email:{type:String,required:true},
    profilePic:{type:String,default:''}
});
userSchema.pre('save',function(next){
    var user=this;
    if(!user.isModified('password')) next();
    bcrypt.hash(user.password,null,null,function(err,hash){
        if (err) return next(err);
        user.password=hash;
        next();
    });
});

userSchema.methods.comparePassword=function(password){
    var user=this;
    return bcrypt.compareSync(password,user.password);
}

module.exports=mongoose.model('User',userSchema);
