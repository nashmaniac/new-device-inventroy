/**
 * Created by asci on 1/18/16.
 */
var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var designationSchema=new Schema({
    name:{type:String,required:true,index:{unique:true}}
});

module.exports=mongoose.model('Designation',designationSchema);