/**
 * Created by asci on 1/20/16.
 */
var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var dataTypeSchema=new Schema({
    dataTypeName:{type:String,required:true,index:{unique:true}},
    textBoxType:{type:String, required:true},
    selectValue:{type:Array,default:[]}
});

module.exports=mongoose.model('dataType',dataTypeSchema);