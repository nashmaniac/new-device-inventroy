/**
 * Created by asci on 1/19/16.
 */
var mongoose=require('mongoose');

var Schema=mongoose.Schema;

var deviceSchema=new Schema({
    as_id:{type:String,required:true,index:{unique:true}},
    type:{type:Schema.Types.ObjectId,ref:'deviceType',require:true},
    registered_by:{type:Schema.Types.ObjectId,ref:'User',required:true},
    registered_date:{type:Date,default:Date.now},
    borrowStatus:{type:Boolean,default:false},
    lost:{type:Boolean,default:false},
    properties:{type:Object,default:{}},
    location:{type:Schema.Types.ObjectId,ref:'location'}
});

module.exports=mongoose.model('Device',deviceSchema);
