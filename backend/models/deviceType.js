/**
 * Created by asci on 1/19/16.
 */
var mongoose=require('mongoose');
var Schema= mongoose.Schema;

var deviceTypeSchema=new Schema({
    typeName:{type:String,index:{unique:true},required:true},
    custom_fields:[{
        name:String,
        fieldType:{type:Schema.Types.ObjectId,ref:'dataType'}
    }]
});

module.exports=mongoose.model('deviceType',deviceTypeSchema)