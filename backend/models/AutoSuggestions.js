var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var autoSuggestionsSchema=new Schema({
  key:{type:String,required:true},
  value:{type:String,required:true}
});

autoSuggestionsSchema.index({key:1,value:1},{unique:true});
module.exports= mongoose.model('autoSuggestions',autoSuggestionsSchema);
