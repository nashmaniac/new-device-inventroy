/**
 * Created by asci on 1/17/16.
 */
var mongoose=require('mongoose');
var autoSuggestions=require('./models/AutoSuggestions');
var User=require('./models/users');
var Designation=require('./models/designation');
var deviceType=require('./models/deviceType');
var Device=require('./models/device');
var borrowEntry=require('./models/borrowEntry');
var location=require('./models/location');
var randomString=require('randomstring');
var jsonwebtoken=require('jsonwebtoken');
var dataType=require('./models/dataType');
var server_config=require('../server_config');
var emailModule=require('./emailModule');
var Excel=require('exceljs');
var fs=require('fs');
var http = require('http');
var path=require('path');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var csv=require('csv-parser');
function mapReduceTask(res,status,message){
  var o={};
  o.map=function(){
    var Obj={}
    for(var e in this.properties){
      Obj[e]=this.properties[e];
    }
    if(this.borrowStatus){
      Obj['Borrowed']='yes';
    }else{
      Obj['Borrowed']='no';
    }
    if(this.lost){
      Obj['Lost']='yes';
    }else{
      Obj['Lost']='no';
    }
    emit(this._id,Obj);
  };
  o.reduce=function(key,arraValues){
    return Array.sum(arraValues);
  };
  var o1={};
  o1.map=function(){
    emit(this.location,1);
  };
  o1.reduce=function(k,values){
    return values.length
  };

  var o2={};

  o2.map=function(){
    emit(this.type,1);
  }
  var o3={};
  o3.map=function(){
    emit(this.registered_by,1);
  }
  Device.mapReduce(o,function(err,output,stats){
    var flattened = output.reduce(function(a, b) {
      return a.concat(b.value);
    }, []);
    Device.mapReduce(o1,function(err1,_d){
      var t=_d.reduce(function(a,b){
        return a.concat(b._id)
      },[]);
      var _t=[];
      location.find({}).where('_id').in(t).exec(function(err,l){
        for(var e in l){
          _t.push({'Location':l[e].place_name+' , '+l[e].country})
        }
        var with_location=_t.reduce(function(a,b){
          return a.concat(b)
        },flattened);
        Device.mapReduce(o2,function(err2,_d){
          var typeId=_d.reduce(function(a,b){
            return a.concat(b._id)
          },[]);
          var typeDetail=[];
          deviceType.find({}).where('_id').in(typeId).exec(function(err,l){
            for(var e in l){
              typeDetail.push({'Device Type':l[e].typeName})
            }
            var with_type=typeDetail.reduce(function(a,b){
              return a.concat(b)
            },with_location);
            Device.mapReduce(o3,function(err3,_d){
              var userId=_d.reduce(function(a,b){
                return a.concat(b._id)
              },[]);
              var userDetail=[];
              User.find({}).where('_id').in(userId).exec(function(err,l){
                for(var e in l){
                  userDetail.push({'Registered by':l[e].name})
                }
                var with_user=userDetail.reduce(function(a,b){
                  return a.concat(b)
                },with_type);
                var borrow_user=[]
                borrowEntry.find({}).populate('borrower').exec(function(err,l){
                  for(var e in l){
                    borrow_user.push({'Borrowed by':l[e].borrower.name});
                  }
                  var with_all=borrow_user.reduce(function(a,b){
                    return a.concat(b);
                  },with_user);
                  autoSuggestions.remove({},function(err){
                    autoSuggestions.insertMany(uniqueKeys(with_all),function(err,data){
                      console.log(data.length);
                      console.log('done');
                      res.json({
                          success:status,
                          message:message
                      });
                    });
                  });
                })
              });
            });
          });
        });
      });
    });
  });
}
function createwebtoken(u,i){
    if(i==null){
        var d="";
    }else{
        var d= i.name;
    }

    if(u.name==''|| u.name==undefined || u.name==null){
        var surname= u.username;
    }else{
        var surname=u.name.split(' ')[0].trim();
    }
    console.log(surname);
    var token=jsonwebtoken.sign({
        id:u._id,
        username: u.username,
        name: u.name,
        surname: surname,
        email: u.email,
        designation: d,
        adminLevel: u.adminLevel,
        profilePic:u.profilePic
    },server_config.secretKey,{
        expiresInMinute:1440
    });
    return token;
}
function uniqueKeys(objects) {
    var keys = objects.reduce(function (collector, obj) {
        Object.keys(obj).forEach(function (key) {
            var t=collector.filter(function(el){
                return el.key==key && el.value==obj[key];
            });
            if(t.length==0){
                collector.push({key:key,value:obj[key]});
            }
        });
        return collector;
    }, []);
    return keys;
}
module.exports=function(app,express,io){
    var api=express.Router();

    api.post('/add_designation',function(req,res){
        var des=new Designation({
            name:req.body.designation
        });
        des.save(function(err,d){
            if(err){
                console.log(err);
                res.json({
                    success:false,
                    message:err
                });
            }else{
                res.json({
                    success:true,
                    message:'Designation is added successfully'
                });
            }

        });
    });
    api.get('/get_designation',function(req,res){
        Designation.find({},function(err,des){
            if(err){
                console.log(err);
            }
            res.json(des);
        })
    });
    api.get('/get_one_designation',function(req,res){
        Designation.findOne({name:req.query.name},function(err,des){
            if(err){
                res.send(err);
            }else{
                res.send(des);
            }
        });
    });
    api.post('/signup',function(req,res){
        var r=randomString.generate();
        console.log(r);
        //get the username
        var username=req.body.email.split("@")[0].trim();
        var u=new User({
            username:username,
            email:req.body.email,
            password:r,
            name:req.body.name
        });
        u.save(function(err,_u){
            var message="<html><table>" +
                "<tr><td>Username:<td><td><b>"+username+"</b></td></tr>" +
                "<tr><td>Password:<td><td><b>"+r+"</b></td></tr>" +
                "<tr><td>Address:<td><td><b>"+req.headers['referer']+"</b></td></tr>" +
                "</table></html>";
            if(err){
                console.log(err);
                res.json({
                    success:false,
                    message:"Request Sign Up failed."
                });
            }else{
                res.json({
                    success:true,
                    message:"A mail has been sent to you. Check your email "
                });
                emailModule.sendMail(server_config.adminAccount,req.body.email,'','User Registration',message);
            }
        });
    });
    api.post('/login',function(req,res){
        User.findOne({username:req.body.username})
            .select('name username password designation email adminLevel profilePic').exec(function(err,u){
                if(err) throw err;
                if(!u){
                    res.send({
                        success:false,
                        message:"User does not exist"
                    });
                }else{
                    var validPassword= u.comparePassword(req.body.password);
                    if(!validPassword){
                        res.send({
                            success:false,
                            message:"Invalid password"
                        });
                    }else{
                        Designation.findOne({_id: u.designation},function(err,i){
                            if(err){
                                res.send({
                                    success:false,
                                    message:"unable to decode user info"
                                });
                            }else{
                                var v=createwebtoken(u,i);
                                res.send({
                                    success:true,
                                    token:v,
                                    message:"Successfully logged in"
                                });
                            }
                        });
                    }
                }
            });
    });
    api.post('/sendPasswordToMail',function(req,res){
      User.findOne({username:req.body.username}).exec(function(err,u){
        var r=randomString.generate();
        u.password=r;
        u.save(function(err,_u){
            var message="<html><table>" +
                "<tr><td>Username:<td><td><b>"+req.body.username+"</b></td></tr>" +
                "<tr><td>Password:<td><td><b>"+r+"</b></td></tr>" +
                "<tr><td>Address:<td><td><b>"+req.headers['referer']+"</b></td></tr>" +
                "</table></html>";
            if(err){
                console.log(err);
                res.json({
                    success:false,
                    message:"Request Sign Up failed."
                });
            }else{
                res.json({
                    success:true,
                    message:"Password has been changed. Check your email "
                });
                emailModule.sendMail(server_config.adminAccount,u.email,'','Password Changed',message);
            }
        });
      });
    });
    //custom middleware

    api.use(function(req,res,next){
        var token=req.headers['x-access-token']||req.body.token;
        if(token){
            jsonwebtoken.verify(token,server_config.secretKey,function(err,decoded){
                if(err){
                    res.status(403).send({success:false,message:'Failed to authenticate user. Forbidden'});
                }else{
                    req.decoded=decoded;
                    next();
                }
            });
        }else{
            res.status(403).send({message:'No token'});
        }
    });

    api.get('/all_users',function(req,res){
        User.find({}).populate('designation').exec(function(err,u){
            if(err){
                console.log(err);
                res.json(err);
            }else{
                res.json(u);
            }
        });
    });

    api.get('/me',function(req,res){
        res.json(req.decoded);
    });

    api.post('/addDeviceType',function(req,res){
        var d=new deviceType({
            typeName:req.body.deviceType,
            custom_fields:req.body.customField
        });
        d.save(function(err,_d){
            if(err){
                res.json({
                    success:false,
                    message:'Device type not added'
                });
            }else{
                res.json({
                    message:"Device type added successfully",
                    success:true
                });
            }
        });
    });
    api.get('/getAllDeviceType',function(req,res){
        deviceType.find({}).populate('custom_fields.fieldType').exec(function(err,d){
            if(err){
                res.json(err)
            }else{
                res.json(d);
            }
        });
    });
    api.post('/addDevice',function(req,res){
        //generate random string
        var _random=randomString.generate({
            length: 6,
            charset: 'numeric'
        });
        var d=new Device({
            as_id:'AS-'+_random,
            type: req.body.type,
            registered_by:req.decoded.id,
            properties:req.body.properties,
            location:req.body.location
        });
        d.save(function(err,_d){
            if(err){
                res.json({
                    success:false,
                    message:'Device not added'
                })
            }else{
                Device.findOne({as_id:_d.as_id})
                    .populate('type')
                    .populate('registered_by')
                    .exec(function(err,de){
                        if(err){
                            res.json(err);
                        } else{
                            io.emit('device',de);
                            mapReduceTask(res,true,"Device is added successfully");
                        }
                    });
            }
        });
    });
    api.get('/getDevice',function(req,res){
        if(Object.keys(req.query).length==0){
          borrowEntry.find({}).populate('borrower').populate('device').exec(function(err,borrowed_device){
            var borrow_list={};
            for(var e in borrowed_device){
              if(borrowed_device[e].borrower.name!=null && borrowed_device[e].borrower.name!=undefined){
                borrow_list[borrowed_device[e].device.as_id]=borrowed_device[e].borrower.name;
              }
            };
            Device.find({})
                .populate('type')
                .populate('location')
                .populate('registered_by')
                .exec(function(err,d){
                    if(err){
                        res.json(err);
                    } else{
                        res.json({
                          devices:d,
                          borrow:borrow_list
                        });
                    }
                });
          });
        }else{
            var t=[];
            if(typeof(req.query.filter)=='string'){
                t.push(req.query.filter);
                req.query.filter=t;
                t=[];
            }
            for(var i=0;i<req.query.filter.length;i++){
              t.push(JSON.parse(req.query.filter[i]));
            }
            var filterOperation=[];
            for(var i=0;i<t.length;i++){
              if(i!=0){
                filterOperation.push(t[i].filter);
              }
            }
            borrowEntry.find({}).populate('borrower').populate('device').exec(function(err,borrowed_device){
              // console.log(borrowed_device);
              var borrow_list={};
              for(var e in borrowed_device){
                if(borrowed_device[e].borrower.name!=null && borrowed_device[e].borrower.name!=undefined){
                  borrow_list[borrowed_device[e].device.as_id]=borrowed_device[e].borrower.name;
                }
              };
              Device.find({}).populate('type').populate('location').populate('registered_by').exec(function(err,d){
                var final=[];
                for(var e in t){
                  console.log(t[e]);
                  var _t=[];
                  if(t[e].key=='Registered by'){
                    _t=d.filter(function(el){
                      if(el.registered_by!=undefined || el.registered_by!=null){
                        return  el.registered_by.name==t[e].value;
                      }
                    });
                  }
                  else if(t[e].key=='Device Type'){
                    _t=d.filter(function(el){
                      return el.type.typeName==t[e].value;
                    });
                  }
                  else if(t[e].key=='Location'){
                    _t=d.filter(function(el){
                      return el.location.place_name==t[e].value.split(',')[0].trim() && el.location.country==t[e].value.split(',')[1].trim();
                    });
                  }
                  else if(t[e].key=='Borrowed'){
                    _t=d.filter(function(el){
                      if(t[e].value=='yes'){
                        return el.borrowStatus==true;
                      }else{
                        return el.borrowStatus==false;
                      }
                    });
                  }
                  else if(t[e].key=='Lost'){
                    _t=d.filter(function(el){
                      if(t[e].value=='yes'){
                          return el.lost==true;
                      }else{
                          return el.lost==false;
                      }
                    });
                  }
                  else if(t[e].key=='Borrowed by'){
                    _b=borrowed_device.filter(function(el){
                      return el.borrower.name==t[e].value;
                    });
                    var device_list=[];
                    for(e in _b){
                      device_list.push((_b[e].device.as_id).toString());
                    }
                    _t=d.filter(function(el){
                      return device_list.indexOf(el.as_id)!=-1;
                    });
                  }
                  else{
                    _t=d.filter(function(el){
                      return el.properties[t[e].key]==t[e].value;
                    });
                  }
                  if(e==0){
                    final=_t;
                  }else{
                    if(filterOperation[e-1]=='AND'){
                      final=final.filter(function(ef){
                        return _t.indexOf(ef)!=-1;
                      });
                    }else{
                      var temp=_t.filter(function(ef){
                        return final.indexOf(ef)==-1;
                      });
                      console.log(temp.length);
                      for(var i=0;i<temp.length;i++){
                        final.push(temp[i]);
                      }
                    }
                  }
                }
                if(err){
                    res.json(err);
                } else{
                    res.json({
                      devices:final,
                      borrow:borrow_list
                    });
                }
              });
            });
        }
    });
    api.post('/addData',function(req,res){
        var d=new dataType({
            dataTypeName:req.body.dataTypeName,
            textBoxType:req.body.textBoxType,
            selectValue:req.body.selectValue
        });
        d.save(function(err,_d){
            console.log(_d);
            if(err){
                res.json({
                    success:false,
                    message:'Data Type is not added.'
                });
            }else{
                res.json({
                    success:true,
                    message:'Data Type is added successfully'
                })
            }

        });
    });
    api.get('/allDataType',function(req,res){
        dataType.find({},function(err,d){
            if(err){
                console.log(err);
                res.json(err);
            }else{
                res.json(d);
            }
        });
    });
    api.get('/deviceDetail/:id',function(req,res){
        Device.find({
            as_id:req.params.id
        }).populate('type')
            .populate('location')
            .populate('registered_by').exec(function(err,data){
                console.log(data);
                if(err){
                    res.json(err);
                }else{
                    res.json(data[0]);
                }
            });
    });

    api.get('/getDeviceInfoForCheckoutFilter/:filter',function(req,res){
        var tag=req.params.filter;
        if(tag=='false'){
            var s=false;
        }else{
            var s=true;
        }
        Device.find({borrowStatus:s},{as_id:1,_id:1,type:1}).populate('type','typeName').exec(function(err,data){
            //console.log(data);
            if(err){
                res.json([])
            }
            var t=[];
            for(var i=0;i<data.length;i++){
                if(data[i].type){
                  t.push({
                      _id:data[i]._id,
                      as_id:data[i].as_id,
                      typeName:data[i].type.typeName
                  });
                }
            }
            res.json(t);
        });
    });
    api.post('/submitBorrowEntry',function(req,res){
        Device.findOne({_id:req.body.device_id},{borrowStatus:1},function(err,data){
            if(!data.borrowStatus){
                var b=new borrowEntry({
                    borrower:req.body.user_id,
                    device:req.body.device_id,
                    borrow_date:req.body.currentDate,
                    expected_return:req.body.finalDate,
                    borrow_comment:req.body.borrow_comment,
                    in_charge:req.decoded.id
                });
                b.save(function(err,borrow){
                    if(err){
                        res.json({
                            success:false,
                            message:'Device checkout unsuccessful'
                        });
                    }
                    //change the borrowStatus of the device
                    data.borrowStatus=true;
                    data.save();

                    console.log("borrowed");
                    mapReduceTask(res,true,"Device checkout successful");
                 });
            }else{
                res.json({
                    success:false,
                    message:'Device is already borrowed'
                })
            }
        });

    });

    api.get('/getAllBorrowEntry',function(req,res){
        borrowEntry.find({}).exec(function(err,b){
            if(err){
                res.json(err);
            }
            res.json(b);
        });
    });

    api.get('/getBorrowInfoForDevice/:device_id',function(req,res){
        borrowEntry.find({device:req.params.device_id})
            .populate('borrower')
            .populate('device')
            .populate('in_charge')
            .sort({borrow_date:-1}).exec(function(err,data){
                if(data.length){
                    res.json(data[0]);
                }else{
                    res.json([]);
                }
            });
    });

    api.post('/deviceCheckIn',function(req,res){
        borrowEntry.findOne({
            _id:req.body.borrow_id,
            device:req.body.device_id
        },function(err,d){
            d.lost=req.body.lost;
            d.return_comment=req.body.return_comment;
            d.actual_return=req.body.return_date;
            d.received_by=req.decoded.id;
            d.save();
            console.log('saved the checkin object.')
            //get the device id
            Device.findOne({_id:req.body.device_id},function(e,dev){
                if(e){
                    res.json({
                        success:false,
                        message:'Device checkin unsuccessful'
                    })
                }
                if(req.body.lost){
                    dev.lost=true;
                }
                dev.borrowStatus=false;
                dev.save();

                res.json({
                    success:true,
                    message:'Device checkin successful'
                });
            });
        });
    });

    api.get('/getBorrowHistory/:device_id',function(req,res){
        var device_id=req.params.device_id;
        Device.findOne({as_id:device_id},function(err,d){
            borrowEntry.find({device: d._id})
                .populate('borrower')
                .exec(function(e,data){
                if(e){
                    res.json([]);
                }
                res.json(data.reverse());
            })
        });
    });

    api.get('/getBorrowDetail/:borrow_id',function(req,res){
        var borrow_id=req.params.borrow_id;
        borrowEntry.find({_id:borrow_id})
            .populate('borrower')
            .populate('device')
            .populate('in_charge')
            .populate('received_by')
            .exec(function(err,b){
                if(err){
                    console.log(err);
                }
                res.json(b);
            });
    });

    api.post('/addLocation',function(req,res){
        var l=new location(req.body);
        l.save(function(err,d){
            if(err){
                console.log(err);
                res.json({
                    success:false,
                    message:'Location add failed'
                });
            }
            res.json({
                success:true,
                message:'Location added successfully'
            });
        })
    });
    api.get('/getLocation',function(req,res){
        location.find({}).populate('in_charge').exec(function(err,d){
            if(err){
                console.log(err);
                res.json([]);
            }
            res.json(d);
        });
    });
    api.get('/autoSuggestions',function(req,res){
      autoSuggestions.find({},{key:1,value:1}).exec(function(err,_d){
        if(err){
          console.log(err);
          res.json([]);
        }
        res.json(_d);
      });
    });
    api.get('/getBorrowedDevice',function(req,res){
        borrowEntry.find({
            borrower:req.query.user_id
        },{device:1}).populate({path:'device',select:'borrowStatus'}).exec(function(err,data){
            if(err){
                console.log(err);
                res.json(err);
            }
            var t=data.filter(function(el){
                return el.device.borrowStatus==true;
            });
            var all_device=[];
            for(var i=0;i< t.length;i++){
                all_device.push(t[i].device._id);
            }
            Device.find().where('_id').in(all_device).populate('location').populate('registered_by').populate('type').exec(function(e,d){
                if(e){
                    console.log(e);
                    res.json(e);
                }
                console.log(d);
                res.json(d);
            })

        });
    });
    api.get('/getDeviceByLocation',function(req,res){
        Device.find({location:req.query.location_id})
            .populate('type')
            .populate('location')
            .populate('registered_by')
            .exec(function(err,data){
                console.log(data);
                if(err){
                    console.log(err);
                    res.json(err);
                }
                res.json(data);
            });
    });
    api.post('/editDevice',function(req,res){
        Device.findOne({as_id:req.body.device_id}).exec(function(err,data){
            if(err){
                console.log(err);
                res.json({
                    success:false,
                    message:"Device not updated sucessfully"
                });
            }
            data.properties=req.body.properties;
            data.type=req.body.type;
            data.location=req.body.location;
            data.save();
            mapReduceTask(res,true,"Device is updated sucessfully")

        });
    });
    api.get('/getSpecificDeviceType',function(req,res){
        deviceType.findOne({_id:req.query.deviceTypeId}).exec(function(err,data){
            if(err){
                console.log(err);
                res.json(err);
            }
            res.json(data);
        });
    });
    api.post('/editNewDeviceType',function(req,res){
        deviceType.findOne({_id:req.body._id}).exec(function(err,data){
            if(err){
                console.log(err);
                res.json({
                    success:false,
                    message:'Device type is not updated successfully'
                });
            }
            data.custom_fields=req.body.customField;
            data.typeName=req.body.deviceType;
            data.save();
            res.json({
                success:true,
                message:'Device type is updated successfully'
            });
        });
    });
    api.post('/changePassword',function(req,res){
        console.log(req.body);
        User.findOne({
            _id:req.decoded.id
        }).select('name username password designation email adminLevel').exec(function(err,data){
            if(err){
                console.log(err);
                res.json({
                    success:false,
                    message:'No user found'
                });
            }
            var valid=data.comparePassword(req.body.old);
            console.log(valid);
            if(valid){
                data.password=req.body.new;
                console.log(data);
                data.save(function(e,u){
                    if(e){
                        console.log(e);
                        res.json({
                            success:false,
                            message:"User password change failed"
                        });
                    }
                    res.json({
                        success:true,
                        message:"User Password Change successful"
                    });
                })
            }else{
                res.json({
                    success:false,
                    message:'Password is invalid'
                });
            }

        });
    });
    api.get('/export',function(req,res){
      deviceType.findOne({_id:req.query.data_id}).exec(function(err,data){
        var columns=[];
        for(var e in data.custom_fields){
          columns.push(data.custom_fields[e].name);
        }
        var c=new Date().toDateString().replace(/ /g,'_')+'_'+data.typeName;
        var workbook = new Excel.Workbook();
        workbook.creator = "Me";
        workbook.lastModifiedBy = "Her";
        workbook.created = new Date();
        workbook.modified = new Date();
        var sheet = workbook.addWorksheet(data.typeName);
        var worksheet = workbook.getWorksheet(data.typeName);
        var mandatory_fields=[]
        Device.schema.eachPath(function(path){
          if(path!='properties' && path!='_id'&& path!='__v'){
            mandatory_fields.push({
              header: path.split('_').join(' ').charAt(0,1).toUpperCase()+path.split('_').join(' ').substr(1),
              width: path.split('_').join(' ').length+10
            });
          }
        });
        for(var i=0;i<data.custom_fields.length;i++){
          mandatory_fields.push({
            header:data.custom_fields[i].name,
            width:data.custom_fields[i].name.length+10
          });
        }
        worksheet.columns=mandatory_fields;
        Device.find({type:data._id}).populate('type').populate('registered_by').populate('location').exec(function(err,d){
          var data_columns=[];
          for(var i=0;i<d.length;i++){
            var t=[];
            t.push(d[i].as_id.toString());
            t.push(d[i].type.typeName.toString());
            t.push(d[i].registered_by.name.toString());
            t.push(d[i].registered_date.toDateString());
            t.push(d[i].borrowStatus.toString());
            t.push(d[i].lost.toString());
            t.push(d[i].location.place_name+','+d[i].location.country);
            for(var j=0;j<data.custom_fields.length;j++){
              if(d[i].properties.hasOwnProperty(data.custom_fields[j].name)){
                t.push(d[i].properties[data.custom_fields[j].name].toString());
              }else{
                t.push(" ");
              }
            }
            worksheet.addRow(t);
          }
          var file_path=path.join(server_config.staticFolder+c+'.xlsx');
          workbook.xlsx.writeFile(file_path).then(function(data){
            res.json({
              success:true,
              filename:c+'.xlsx',
              location:c+'.xlsx',
            });
          });
        });
      });
    });
    api.post('/importData',multipartMiddleware,function(req,res){
      console.log(req.files.file.path);
      fs.readFile(req.files.file.path,function(err,data){
        var newPath = server_config.baseFolder + "/"+req.files.file.name;
        fs.writeFile(newPath, data, function (err) {
          var deviceList=[];
          location.find({},{_id:1,place_name:1}).exec(function(err,locationData){
            var LocationObject={};
            for(e in locationData){
              LocationObject[locationData[e].place_name]=locationData[e]._id;
            }
            deviceType.find({},{_id:1,typeName:1}).exec(function(errorData,deviceTypeData){
              var deviceTypeObject={};
              for(e in deviceTypeData){
                deviceTypeObject[deviceTypeData[e].typeName]=deviceTypeData[e]._id;
              }
              console.log(deviceTypeObject);
              var o={};
              o.map=function(){emit(this._id,this.username)};
              /*o.reduce=function(k,values){
                return values.length;
              }*/
              User.mapReduce(o,function(userError,ye){
                  var userObject={};
                  for(e in ye){
                    userObject[ye[e].value]=ye[e]._id;
                  }
                  fs.createReadStream(newPath).pipe(csv()).on('data', function(data) {
                    var deviceObject={};
                    var prop={};
                    var _random=randomString.generate({
                        length: 8,
                        charset: 'numeric'
                    });
                    deviceObject['as_id']='AS-'+_random;
                    deviceObject['registered_by']=userObject[req.decoded.username];
                    deviceObject['type']=deviceTypeObject['Mobile'];
                    for(e in data){
                      if(data[e]==''|| data[e]==undefined|| data[e]==null){
                        delete data[e];
                      }else if(e.trim()=='Received'){
                        var tag=data[e].split('/');
                        deviceObject['registered_date']=new Date(parseInt(tag[2]), parseInt(tag[0]),parseInt(tag[1]));
                      }else if(e.trim()=='Location'){
                        deviceObject['location']=LocationObject[data[e.trim()]];
                      }
                      else{
                        if(e.trim()=='Manf.'){
                          prop['Manufacturer']=data[e];
                        }else{
                          prop[e]=data[e];
                        }
                      }
                    }
                    deviceObject['properties']=prop;
                    deviceList.push(deviceObject);

                  })
                  .on('end',function(d){
                    console.log(deviceList.length);
                    var _d=deviceList.filter(function(e){
                      return (e.location!=undefined && e.location!=null) && (e.type!=null && e.type!=undefined) && (e.registered_by!=null && e.registered_by!=undefined);
                    });
                    //console.log(_d.length);
                    Device.insertMany(_d, function(error, docs) {
                      if(error){
                        console.log(error);
                        res.json({
                          success:false,
                          message:"File Upload Failed"
                        });
                      }else{
                        fs.unlink(newPath,function(err){
                          if(err){
                            console.log('error in deleting',err);
                          }else{
                            console.log('deleted the uploaded file');
                            res.json({
                              success:true,
                              message:"Successfully uploaded the file"
                            });
                          }
                        })
                      }
                    });
                    //console.log(_d.length);
                  });
              });
            });
          });
        });
      });
    });
    api.post('/changeProfilePic',multipartMiddleware,function(req,res){
      fs.readFile(req.files.file.path,function(err,data){
        var newPath = server_config.staticFolder + "css/images/"+req.files.file.name;
        console.log(newPath);
        fs.writeFile(newPath, data, function (err) {
          if(err){
            res.json({
              success:false
            })
          }
          User.findOne({_id:req.decoded.id}).exec(function(err,u){
            u.profilePic=req.files.file.name;
            u.save(function(err,_u){
              console.log(_u);
              req.decoded.profilePic=req.files.file.name;
              res.json({
                success:true,
                message:'Profile picture saved successfully'
              })

            });
          });

        });
      });
    });
    api.get('/getUserDetail',function(req,res){
      var user_id=req.query.user_id;
      User.findOne({_id:user_id}).populate('designation').exec(function(err,d){
        if(err){
          res.json({
            user:{},
            borrow:[]
          });
        }else{
          borrowEntry.find({borrower:user_id}).populate('device').populate('borrower').exec(function(e,b){
            if(e){
              res.json({
                user:d,
                borrow:[]
              });
            }
            console.log(b);
            res.json({
              user:d,
              borrow:b
            });
          });


        }
      })
    });
    return api;
};
