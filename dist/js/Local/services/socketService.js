/**
 * Created by asci on 1/19/16.
 */
(function(){
    var socketService=angular.module('socketService',[]);
    socketService.factory('socketio',function($rootScope){
        var vm={};
        var socket=io.connect();
        vm.on=function(eventName,callback){
            socket.on(eventName,function(){
                var args=arguments;
                $rootScope.$apply(function(){
                    callback.apply(socket,args);
                });
            });
        };
        vm.emit=function(eventName,data,callback) {
            socket.emit(eventName,data,function(){
                var args=arguments;
                $rootScope.apply(function(){
                    if(callback){
                        callback.apply(socket,args);
                    }
                });
            });
        };
        return vm;
    });
})();