/**
 * Created by asci on 1/18/16.
 */
(function(){
    var signupService=angular.module('signupService',[]);
    signupService.factory('signupFactory',function($http){
        var signupFact={};
        signupFact.getAllDesignation=function(){
            return $http.get('/api/get_designation');
        };
        signupFact.getSpecificDesignation=function(designation){
            return $http.get('/api/get_one_designation',{params:{name:designation}});
        };
        signupFact.addUser=function(userData){
            return $http.post('/api/signup',userData);
        };
        return signupFact;
    });
})();