/**
 * Created by asci on 1/18/16.
 */
(function(){
    var app=angular.module("myApp",['ngSanitize','ui.select','routeApp','ngNotify','socketService','GlobalUsage','signupController','loginController','deviceController','userController']);
    app.config(function($httpProvider){
        $httpProvider.interceptors.push('AuthIntercept');
    });
})();
