/**
 * Created by asci on 1/19/16.
 */
(function(){
    var loginController=angular.module('loginController',['loginService']);
    loginController.controller('loginCtrl',function(Auth,$location,$rootScope,ngNotify){
        var vm=this;
        vm.resetPass=false;
        vm.loggedIn=Auth.isLoggedIn();
        if(!vm.loggedIn){
            $location.path('/login');
        }
        // else {
        //   $location.path('/');
        // }
        $rootScope.$on('$locationChangeStart',function(){
            vm.loggedIn=Auth.isLoggedIn();
            Auth.getUser()
                .success(function(data){
                    vm.user=data;
                });
        });

        vm.submitLogin=function(){
            Auth.login(vm.loginData)
                .success(function(data){
                    if(data.success){
                        ngNotify.set(data.message,{
                            type:'success'
                        });
                        $location.path('/');
                    }else{
                        ngNotify.set(data.message,{
                            type:'error'
                        });
                        $location.path('/login');
                    }
                });
        };
        vm.logOut=function(){
            Auth.logout();
            ngNotify.set("Logged Out Successfully",{
                type:'success'
            });
            $location.path('/login');
        };
        vm.resendPass=function(){
          vm.resetPass=!vm.resetPass;
        };
        vm.resetPassword=function(){
          Auth.resetPasswordToEmail(vm.resendUser).success(function(data){
            if(data.success){
                ngNotify.set(data.message,{
                    type:'success'
                });
                $location.path('/');
            }else{
                ngNotify.set(data.message,{
                    type:'error'
                });
                $location.set('/signup')
            }
          });
        };
    });
})();
