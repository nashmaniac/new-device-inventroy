/**
 * Created by asci on 1/18/16.
 */
(function(){
    var signupController=angular.module('signupController',['signupService']);
    signupController.controller('signupCtrl',function(signupFactory,$location,ngNotify){
        var vm=this;
        signupFactory.getAllDesignation()
            .success(function(data){
                vm.designation=data;
            });

        vm.signupSubmit=function(){
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@assetscience.com/;
            if(re.test(vm.signupData.email)){
                console.log('valid email');
                signupFactory.addUser(vm.signupData)
                    .success(function(data){
                        if(data.success){
                            ngNotify.set(data.message,{
                                type:'success'
                            });
                            $location.path('/');
                        }else{
                            ngNotify.set(data.message,{
                                type:'error'
                            });
                            $location.set('/signup')
                        }
                    });
            }else{
                ngNotify.set('You do not have Asset Science Autorized Mail',{
                    type:'info'
                });
            }
        };
    })
})();