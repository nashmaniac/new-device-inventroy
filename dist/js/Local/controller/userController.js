/**
 * Created by asci on 1/19/16.
 */
(function(){
    var userController=angular.module('userController',['userService','GlobalUsage'])
    userController.controller('userCtrl',function(Auth,userServiceFactory,socketio){

        var vm=this;
        vm.currentPage=1;
        vm.pageLimit="5";
        vm.loading=true;
        vm.filteredUsers=[];
        userServiceFactory.getAllUser()
            .success(function(data){
                vm.userCol=["Username","Name","Email","Designation","Admin"];
                vm.users=data;
                vm.doPaging();
            });
        vm.changePageLimit=function(){
          vm.currentPage=1;
          vm.loading=true;
          vm.doPaging();
        }
        vm.doPaging=function(){
          var s=(parseInt(vm.currentPage)-1)*parseInt(vm.pageLimit);
          var e=s+parseInt(vm.pageLimit);
          vm.loading=false;
          vm.filteredUsers=vm.users.slice(s,e);
        };
        vm.pageItem=function(page){
          vm.currentPage=page;
          vm.loading=true;
          vm.doPaging();
        }
        socketio.on('user',function(data){
           vm.users.push(data);
        });
    });
    userController.controller('profilePic',function($scope,fileUpload,ngNotify){
      var vm=this;
      vm.uploadProfilePic=function(){
        var file = $scope.myFile;
        var uploadUrl = '/api/changeProfilePic';
        fileUpload.uploadFileToUrl(file, uploadUrl).success(function(data){
          if(data.success){
            ngNotify.set(data.message,{
              type:'success'
            });
          }else{
            ngNotify.set(data.message,{
              type:'error'
            })
          }
          $location.path('/account');
        });
      };
    });
    userController.controller('passwordController',function(userServiceFactory,ngNotify){
        var vm=this;
        vm.clicked=false;
        vm.toggleClick=function(){
            vm.clicked=!vm.clicked;
        }
        vm.changePassword=function(){
            if(vm.changePass.new!=vm.changePass.confirm){
                ngNotify.set("Password don't match",{
                    type:'error'
                });
            }
            userServiceFactory.PasswordChange(vm.changePass).success(function(data){
                if(data.success){
                    ngNotify.set(data.message,{
                        type:'success'
                    });
                }else{
                    ngNotify.set(data.message,{
                        type:'error'
                    });
                }
                vm.toggleClick();
                vm.changePass={};
            });

        };
    });
    userController.controller('userDetailController',function($stateParams,userServiceFactory){
      var vm=this;
      vm.user_id=$stateParams['user_id'];
      vm.user={};
      vm.currentPage=1;
      vm.pageLimit='10';
      vm.loading=true;
      vm.borrowColumn=['Borrow Id','Device Id','Borrow Date','Expected Return Date'];
      userServiceFactory.getUserDetail(vm.user_id).success(function(data){
        vm.user=data['user'];
        vm.borrowHistory=data['borrow'];
        vm.borrowPaging();
      });
      vm.borrowPaging=function(){
        var s=(parseInt(vm.currentPage)-1)*parseInt(vm.pageLimit);
        var e=parseInt(vm.pageLimit)+s;
        vm.loading=false;
        vm.filteredBorrow=vm.borrowHistory.slice(s,e);
      };
      vm.pageItem=function(page){
        vm.currentPage=page;
        vm.loading=true;
        vm.borrowPaging();
      };
      vm.changePageLimit=function(){
        vm.currentPage=1;
        vm.loading=true;
        vm.borrowPaging();
      }
    });
})();
